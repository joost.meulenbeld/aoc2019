include("../lib.jl")

function intcode(memory::Vector{Int}, input::Int)
    index = 1
    output = -1
    while true
        instruction = lpad(memory[index], 5, "0")
        opcode = instruction[end - 1:end]
        instruction_and_params = Int[]
        if opcode == "99"
            break
        elseif opcode == "01"
            instruction_and_params = memory[index:index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : memory[instruction_and_params[3] + 1]
            memory[instruction_and_params[4] + 1] = operand1 + operand2
        elseif opcode == "02"
            instruction_and_params = memory[index:index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : memory[instruction_and_params[3] + 1]
            memory[instruction_and_params[4] + 1] = operand1 * operand2
        elseif opcode == "03"
            instruction_and_params = memory[index:index + 1]
            memory[instruction_and_params[2] + 1] = input
        elseif opcode == "04"
            instruction_and_params = memory[index:index + 1]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            instruction_and_params = memory[index:index + 1]
            output = operand1
        elseif opcode == "05" || opcode == "06"
            instruction_and_params = memory[index:index + 2]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : memory[instruction_and_params[3] + 1]
            if (opcode == "05" && operand1 != 0) || (opcode == "06" && operand1 == 0)
                index = operand2 + 1
                instruction_and_params = Int[]
            end
        elseif opcode == "07"
            instruction_and_params = memory[index:index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : memory[instruction_and_params[3] + 1]
            memory[memory[index + 3] + 1] = Int(operand1 < operand2)
        elseif opcode == "08"
            instruction_and_params = memory[index:index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : memory[instruction_and_params[3] + 1]
            memory[memory[index + 3] + 1] = Int(operand1 == operand2)
        else
            throw(ErrorException("Instruction $instruction is not valid"))
        end
        index += length(instruction_and_params)
    end
    return output
end

run_intcode(input_instructions::AbstractString, input::Int) = intcode(parse.(Int, split(input_instructions, ",")), input)

do_1(fname::String, input::Int) = run_intcode(strip(read(fname, String)), input)

fname = "day_05/input.txt"

# @time println("Anser for part one: $(do_1(fname, 1))")
# @time println("Anser for part two: $(do_1(fname, 5))")
