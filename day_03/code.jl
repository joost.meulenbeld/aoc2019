struct Instruction
    direction::Char
    amount::Int
end
Instruction(s::AbstractString) = Instruction(s[1], parse(Int, s[2:end]))
parse_instructions(in_str::AbstractString) = Instruction.(split(in_str, ","))

const Coordinate = Tuple{Int,Int}
const Line = Tuple{Coordinate,Coordinate}


function coordinates_from_instructions(instructions::Vector{Instruction})::Vector{Coordinate}
    coordinates = [(0, 0)]
    directions = Dict('U' => (0, 1), 'R' => (1, 0), 'D' => (0, -1), 'L' => (-1, 0))
    for instruction = instructions
        delta = directions[instruction.direction] .* instruction.amount
        new_coordinate = coordinates[end] .+ delta
        push!(coordinates, new_coordinate)
    end
    return coordinates
end

horizontal(line::Line) = line[1][2] == line[2][2]
vertical(line::Line) = !horizontal(line)

sort2(x::T, y::T) where {T} = x < y ? (x, y) : (y, x)
function between(p1::T, p2::T, q::T) where {T}
    lower, upper = sort2(p1, p2)
    return lower < q < upper
end

manhattan(coordinate::Coordinate) = sum(abs, coordinate)
manhattan(line::Line) = manhattan(line[2] .- line[1])

function lines_cross(line1::Line, line2::Line)::Coordinate
    if horizontal(line1) == horizontal(line2)
        return (typemax(Int), typemax(Int))
    end
    if vertical(line1)
        line1, line2 = line2, line1
    end
    # Now line1 is horizontal!
    if between(line1[1][1], line1[2][1], line2[1][1]) &&
       between(line2[1][2], line2[2][2], line1[1][2])
       return (line2[1][1], line1[1][2])
    end
    return (typemax(Int), typemax(Int))
end

function get_crossings(instructions1::Vector{Instruction}, instructions2::Vector{Instruction})::Vector{Coordinate}
    trajectory1 = coordinates_from_instructions(instructions1)
    trajectory2 = coordinates_from_instructions(instructions2)
    crossings = Coordinate[]
    for l1 = zip(trajectory1, trajectory1[2:end]), l2 = zip(trajectory2, trajectory2[2:end])
        crossing = lines_cross(l1, l2)
        if crossing != (typemax(Int), typemax(Int)) && crossing != (0, 0)
            push!(crossings, crossing)
        end
    end
    return crossings
end

function get_closest_crossing_along_line(instructions1::Vector{Instruction}, instructions2::Vector{Instruction})::Int
    trajectory1 = coordinates_from_instructions(instructions1)
    trajectory2 = coordinates_from_instructions(instructions2)
    min_distance = typemax(Int)
    distance_along_line1 = 0
    for l1 = zip(trajectory1, trajectory1[2:end])
        distance_along_line2 = 0
        for l2 = zip(trajectory2, trajectory2[2:end])
            crossing = lines_cross(l1, l2)
            if crossing != (typemax(Int), typemax(Int)) && crossing != (0, 0)
                distance_l1 = manhattan((l1[1], crossing))
                distance_l2 = manhattan((l2[1], crossing))
                min_distance = min(min_distance, distance_along_line1 + distance_along_line2 + distance_l1 + distance_l2)
            end
            distance_along_line2 += manhattan(l2)
        end
        distance_along_line1 += manhattan(l1)
        if distance_along_line1 > min_distance || distance_along_line2 > min_distance
            return min_distance
        end
    end
    return min_distance
end

read_puzzle_input(fname::String) = open(fname) do f; return readlines(f) end

function get_closest_crossing_1(puzzle_input1::String, puzzle_input2::String)::Int
    instructions1 = parse_instructions(puzzle_input1)
    instructions2 = parse_instructions(puzzle_input2)
    crossings = get_crossings(instructions1, instructions2)
    minimum_distance = min(manhattan.(crossings)...)
    return minimum_distance
end

function get_closest_crossing_along_line_2(puzzle_input1::String, puzzle_input2::String)::Int
    instructions1 = parse_instructions(puzzle_input1)
    instructions2 = parse_instructions(puzzle_input2)
    minimum_distance = get_closest_crossing_along_line(instructions1, instructions2)
    return minimum_distance
end

function do_1(fname::String)
    puzzle_input = read_puzzle_input(fname)
    return get_closest_crossing_1(puzzle_input[1], puzzle_input[2])
end

function do_2(fname::String)
    puzzle_input = read_puzzle_input(fname)
    return get_closest_crossing_along_line_2(puzzle_input[1], puzzle_input[2])
end

fname = "day_03/input.txt"

# @time println("Answer for part 1: $(do_1(fname))")
# @time println("Answer for part 1: $(do_1(fname))")
@time println("Answer for part 2: $(do_2(fname))")
@time println("Answer for part 2: $(do_2(fname))")
