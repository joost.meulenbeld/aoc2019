include("../lib.jl")


function intcode(memory::Array{Int,1}, operators=[+, *])
    index = 1
    while memory[index] != 99
        op = operators[memory[index]]
        memory[memory[index+3]+1] = op( memory[memory[index+1]+1], memory[memory[index+2]+1] )
        index += 4
    end
    return memory
end


function do_assignment(array::Array{Int, 1}, noun::Int, verb::Int)
    array[2] = noun
    array[3] = verb
    return intcode(array)[1]
end


do_1(fname::String) = do_assignment(read_comma_separated_file(fname), 12, 2)

function do_2(fname::String)
    array = read_comma_separated_file(fname)
    for noun = 0:99, verb = 0:99
        if do_assignment(copy(array), noun, verb) == 19690720
            return noun*100 + verb
        end
    end
end


fname = "day_02/input.txt"

println("Anser for part one: $(do_1(fname))")
println("Anser for part two: $(do_2(fname))")
