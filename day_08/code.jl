function get_layers(input::String, height::Int, width::Int)::Array{String, 3}
    output_shape = (width, height, length(input) ÷ (width * height))
    return permutedims(reshape(split(input, ""), output_shape...), [2, 1, 3])
end

function do_1(input::String, height::Int, width::Int)
    layers = get_layers(input, height, width)
    idx = argmin(sum(layers.=="0", dims=(1, 2)))[3]
    layer_min_number_zeros = layers[:, :, idx]
    return sum(layer_min_number_zeros .== "1") * sum(layer_min_number_zeros .== "2")
end

function fill_image(input::String, height::Int, width::Int)::Array{String, 2}
    layers = get_layers(input, height, width)
    output_image = fill("2", size(layers)[1:2])
    for i = size(layers, 3):-1:1
        mask = layers[:, :, i] .!= "2"
        output_image[mask] = layers[:, :, i][mask]
    end
    return output_image
end


function show_image(image::Array{String, 2})
    image_printable = map(i -> i == "1" ? "█" : " ", image)
    for y = 1:size(image_printable, 1)
        println(join(image_printable[y, :]))
    end
end

fname = "day_08/input.txt"
@time println("Answer part 1: $(do_1(readlines(fname)[1], 6, 25))")
@time println("Answer part 1: $(do_1(readlines(fname)[1], 6, 25))")

show_image(fill_image(readlines(fname)[1], 6, 25))