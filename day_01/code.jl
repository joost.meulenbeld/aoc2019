include("../lib.jl")

calc_fuel(mass::Int) = mass ÷ 3 - 2

function calc_fuel_with_increase(mass::Int)
    total = 0
    prev = mass
    while true
        prev = calc_fuel(prev)
        if prev <= 0
            break
        end
        total += prev
    end
    return total
end

do_1(fname::String) = sum(calc_fuel, read_integers_from_file(fname))
do_2(fname::String) = sum(calc_fuel_with_increase, read_integers_from_file(fname))

fname = "day_01/input.txt"
println("Solution 1: $(do_1(fname))")
println("Solution 2: $(do_2(fname))")
