function is_valid(number::Integer)::Bool
    number_string = string(number)
    return occursin(r"(\d)\1", number_string) &&
        all(l1 <= l2 for (l1, l2) in zip(number_string, number_string[2:end]))
end


function has_exactly_two_consecutive_equal(s::String)::Bool
    if s[1] == s[2] != s[3] || s[end-2] != s[end-1] == s[end]
        return true
    end
    for i = 1:length(s) - 3
        if s[i] != s[i+1] == s[i+2] != s[i+3]
            return true
        end
    end
    return false
end

function is_valid2(number::Integer)::Bool
    number_string = string(number)
    return has_exactly_two_consecutive_equal(number_string) &&
        all(l1 <= l2 for (l1, l2) in zip(number_string, number_string[2:end]))
end


do_1(r1::Integer, r2::Integer) = sum(is_valid, r1:r2)
do_2(r1::Integer, r2::Integer) = sum(is_valid2, r1:r2)

@time println("Part 1 answer: $(do_1(235741, 706948))")
@time println("Part 1 answer: $(do_1(235741, 706948))")
@time println("Part 2 answer: $(do_2(235741, 706948))")
@time println("Part 2 answer: $(do_2(235741, 706948))")
