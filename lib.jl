function read_integers_from_file(fname::String)
    open(fname) do f
        return parse.(Int, readlines(f))
    end
end

function split_string_into_integers(s::String, delimiter=",")
    if length(s) == 0
        return Int[]
    end
    return parse.(Int, split(s, delimiter))
end

function read_comma_separated_file(fname::String, delimiter=",")
    open(fname) do f
        return split_string_into_integers(read(f, String), delimiter)
    end
end
