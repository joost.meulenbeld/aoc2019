mutable struct TreeNode
    name::String
    children::Vector{TreeNode}
    parent::TreeNode
    TreeNode(name::String) = new(name, TreeNode[])
end

function parse_tree(s::Vector{String})::TreeNode
    nodes = Dict{String, TreeNode}()
    nodes["COM"] = TreeNode("COM")

    for item = s
        planet, moon = convert.(String, split(item, ")"))
        if !haskey(nodes, planet)
            nodes[planet] = TreeNode(planet)
        end
        if !haskey(nodes, moon)
            nodes[moon] = TreeNode(moon)
        end
        push!(nodes[planet].children, nodes[moon])
        nodes[moon].parent = nodes[planet]
    end
    return nodes["COM"]
end

get_all_nodes_in_tree(node::TreeNode)::Vector{TreeNode} = [node, Iterators.flatten(map(get_all_nodes_in_tree, node.children))...]
get_all_ancestors(node::TreeNode)::Vector{TreeNode} = node.name == "COM" ? [node] : [get_all_ancestors(node.parent)..., node]
n_all_orbits(top_level_node::TreeNode)::Integer = sum(x -> length(get_all_ancestors(x)) - 1, get_all_nodes_in_tree(top_level_node))
get_node_dict(node::TreeNode)::Dict{String, TreeNode} = Dict(n.name => n for n = get_all_nodes_in_tree(node))


function hops_required(n1::TreeNode, n2::TreeNode)
    ancestors_1 = get_all_ancestors(n1)
    ancestors_2 = get_all_ancestors(n2)
    for i = 1:min(length(ancestors_1), length(ancestors_2))
        if ancestors_1[i] != ancestors_2[i] || i > length(ancestors_1) || i > length(ancestors_2)
            return length(ancestors_1) - i + length(ancestors_2) - i
        end
    end
    return 0
end


# function distance_common_ancestor(, node1::TreeNode, node2::TreeNode)


do_1(fname::String) = open(fname::String) do f; n_all_orbits(parse_tree(readlines(f))) end
do_1(fname::String) = open(fname::String) do f; n_all_orbits(parse_tree(readlines(f))) end
function do_2(fname::String)
    tree = parse_tree(readlines(fname))
    node_dict = get_node_dict(tree)
    return hops_required(node_dict["YOU"], node_dict["SAN"])
end

fname = "day_06/input.txt"

@time println("Answer 1: $(do_1(fname))")
@time println("Answer 2: $(do_2(fname))")
