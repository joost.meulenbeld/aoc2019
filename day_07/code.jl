using Combinatorics

include("../lib.jl")

mutable struct IntcodeComputer
    memory::Vector{Int}
    index::Int
    halted::Bool
    output::Int
    IntcodeComputer(memory::Vector{Int}) = new(memory, 1, false, -1)
end

function run_until_output_or_input(ic::IntcodeComputer, inputs::Vector{Int}, stop_on_output::Bool=false)
    while true
        instruction = lpad(ic.memory[ic.index], 5, "0")
        opcode = instruction[end - 1:end]
        instruction_and_params = Int[]
        if opcode == "99"
            ic.halted = true
            break
        elseif opcode == "01"
            instruction_and_params = ic.memory[ic.index:ic.index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : ic.memory[instruction_and_params[3] + 1]
            ic.memory[instruction_and_params[4] + 1] = operand1 + operand2
        elseif opcode == "02"
            instruction_and_params = ic.memory[ic.index:ic.index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : ic.memory[instruction_and_params[3] + 1]
            ic.memory[instruction_and_params[4] + 1] = operand1 * operand2
        elseif opcode == "03"
            instruction_and_params = ic.memory[ic.index:ic.index + 1]
            ic.memory[instruction_and_params[2] + 1] = popfirst!(inputs)
        elseif opcode == "04"
            instruction_and_params = ic.memory[ic.index:ic.index + 1]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            instruction_and_params = ic.memory[ic.index:ic.index + 1]
            ic.output = operand1
            if stop_on_output
                ic.index += length(instruction_and_params)
                return ic.output
            end
        elseif opcode == "05" || opcode == "06"
            instruction_and_params = ic.memory[ic.index:ic.index + 2]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : ic.memory[instruction_and_params[3] + 1]
            if (opcode == "05" && operand1 != 0) || (opcode == "06" && operand1 == 0)
                ic.index = operand2 + 1
                instruction_and_params = Int[]
            end
        elseif opcode == "07"
            instruction_and_params = ic.memory[ic.index:ic.index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : ic.memory[instruction_and_params[3] + 1]
            ic.memory[ic.memory[ic.index + 3] + 1] = Int(operand1 < operand2)
        elseif opcode == "08"
            instruction_and_params = ic.memory[ic.index:ic.index + 3]
            operand1 = instruction[end - 2] == '1' ? instruction_and_params[2] : ic.memory[instruction_and_params[2] + 1]
            operand2 = instruction[end - 3] == '1' ? instruction_and_params[3] : ic.memory[instruction_and_params[3] + 1]
            ic.memory[ic.memory[ic.index + 3] + 1] = Int(operand1 == operand2)
        else
            throw(ErrorException("Instruction $instruction is not valid"))
        end
        ic.index += length(instruction_and_params)
    end
    return ic.output
end

function run_intcode(memory::Vector{Int}, input::Int)
    ic = IntcodeComputer(copy(memory))
    return run_until_output_or_input(ic, [input])
end

function get_thruster(memory::Vector{Int}, phase_signal::Vector{Int})
    output = 0
    for phase = phase_signal
        ic = IntcodeComputer(copy(memory))
        output = run_until_output_or_input(ic, [phase, output])
    end
    return output
end


function feedback_thruster(memory::Vector{Int}, phase_signal::Vector{Int})
    ics = [IntcodeComputer(copy(memory)) for phase = phase_signal]
    signal = 0
    for i = 1:length(ics)
        signal = run_until_output_or_input(ics[i], [phase_signal[i], signal], true)
    end
    while !ics[end].halted
        for i = 1:length(ics)
            signal = run_until_output_or_input(ics[i], [signal], true)
        end
    end
    return signal
end


maximize_thruster(memory::Vector{Int}) = max((get_thruster(memory, p) for p = permutations(0:4))...)
maximize_feedback_thruster(memory::Vector{Int}) = max((feedback_thruster(memory, p) for p = permutations(5:9))...)



do_1(fname::String) = maximize_thruster(parse.(Int, split(strip(read(fname, String)), ",")))
do_2(fname::String) = maximize_feedback_thruster(parse.(Int, split(strip(read(fname, String)), ",")))

fname = "day_07/input.txt"

@time println("Anser for part one: $(do_1(fname))")
@time println("Anser for part one: $(do_1(fname))")
@time println("Anser for part two: $(do_2(fname))")
@time println("Anser for part two: $(do_2(fname))")
