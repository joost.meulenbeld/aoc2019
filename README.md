Code for [Advent of Code 2019](https://adventofcode.com/), as well as the first time using Julia language (v1.3.0).

To test code, call `julia test/runtests.jl`.