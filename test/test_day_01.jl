import Test: @test, @testset
include("../day_01/code.jl")

@testset "day1" begin
    @testset "calc_fuel" begin
    @test calc_fuel(12) === 2
    @test calc_fuel(14) === 2
    @test calc_fuel(1969) === 654
    @test calc_fuel(100756) === 33583
    end

    @testset "calc_fuel_with_increase" begin
    @test calc_fuel_with_increase(14) === 2
    @test calc_fuel_with_increase(1969) === 966
    @test calc_fuel_with_increase(100756) === 50346
    end
end