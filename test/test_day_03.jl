import Test: @test, @testset
include("../day_03/code.jl")

@testset "day3" begin
    @testset "Instruction" begin
        @test Instruction("U124") == Instruction('U', 124)
        @test parse_instructions("U124,D32") == [Instruction('U', 124), Instruction('D', 32)]
    end
    @testset "coordinates_from_instructions" begin
        instructions = [
            Instruction('U', 5),
            Instruction('D', 4),
            Instruction('R', 3),
            Instruction('L', 2),
            Instruction('R', 3)
        ]
        @test coordinates_from_instructions(instructions) == [(0, 0), (0, 5), (0, 1), (3, 1), (1, 1), (4, 1)]
    end

    @testset "auxiliary" begin
        @test sort2(1, 2) == (1, 2)
        @test sort2(2, 1) == (1, 2)
        @test horizontal(((0, 0), (2, 0)))
        @test vertical(((1, -1), (1, 1)))
        @test between(1, 3, 2)
        @test !between(1, 3, 0)
        @test between(3, 1, 2)
        @test !between(3, 1, 0)
    end

    @testset "lines_cross" begin
        line1 = ((0, 0), (2, 0))
        line2 = ((1, -1), (1, 1))
        line3 = ((1, 1), (1, 2))
        @test lines_cross(line1, line2) == (1, 0)
        @test lines_cross(line2, line1) == (1, 0)
        @test lines_cross(line1, line3) == (typemax(Int), typemax(Int))
        @test lines_cross(line3, line1) == (typemax(Int), typemax(Int))
    end

    @testset "closest_crossing" begin
        @test get_closest_crossing_1("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83") == 159
        @test get_closest_crossing_1("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7") == 135
        @test get_closest_crossing_along_line_2("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83") == 610
        @test get_closest_crossing_along_line_2("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7") == 410
end
end
