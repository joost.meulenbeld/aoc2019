import Test: @test, @testset
include("../day_04/code.jl")

@testset "day4" begin
    @testset "is_valid" begin
        @test is_valid(111111)
        @test !is_valid(223450)
        @test !is_valid(123789)
        @test is_valid2(112233)
        @test !is_valid2(123444)
        @test is_valid2(111122)
    end
end
