import Test: @test, @testset
include("../lib.jl")

@testset "test_lib" begin
    @testset "read_integers_from_file" begin
        @test Base.return_types(read_integers_from_file, (String,)) == [Array{Int, 1}]
        @test read_integers_from_file("test/testfile.txt") == [1, 5, 11]
    end

    @testset "split_string_into_integers" begin
        @test Base.return_types(split_string_into_integers, (String,)) == [Array{Int, 1}]
        @test split_string_into_integers("") == []
        @test split_string_into_integers("1,4,6") == [1, 4, 6]
        @test split_string_into_integers("1.4.6", ".") == [1, 4, 6]
    end

    @testset "read_comma_separated_file" begin
        @test Base.return_types(read_comma_separated_file, (String,)) == [Array{Int, 1}]
        @test read_comma_separated_file("test/testfile2.txt") == [1, 2, 5]
    end
end