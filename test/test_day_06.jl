import Test: @test, @testset
include("../day_06/code.jl")

@testset "day6" begin
    @testset "n_all_orbits" begin
        teststr = """COM)B
        B)C
        C)D
        D)E
        E)F
        B)G
        G)H
        D)I
        E)J
        J)K
        K)L"""
        instructions = convert.(String, split(teststr, "\n"))
        tree = parse_tree(instructions)
        @test length(get_all_nodes_in_tree(tree)) == 12
        @test length(get_all_ancestors(tree)) == 1
        @test n_all_orbits(tree) == 42
        @test do_1(fname) == 278744
    end
    @testset "part2" begin
        teststr = """COM)B
        B)C
        C)D
        D)E
        E)F
        B)G
        G)H
        D)I
        E)J
        J)K
        K)L
        K)YOU
        I)SAN"""
        instructions = convert.(String, split(teststr, "\n"))
        tree = parse_tree(instructions)
        node_dict = get_node_dict(tree)
        @assert hops_required(node_dict["YOU"], node_dict["SAN"]) == 4
    end
end