import Test: @test, @testset
include("../day_08/code.jl")

@testset "day8" begin
    @testset "part1" begin
        test_array = fill("0", 2, 3, 2)
        test_array[:, :, 1] = ["1" "2" "3"; "4" "5" "6"]
        test_array[:, :, 2] = ["7" "8" "9"; "0" "1" "2"]
        @test get_layers("123456789012", 2, 3) == test_array
        @test do_1("123456789012", 2, 3) == 1
    end
end