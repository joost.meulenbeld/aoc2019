import Test: @test, @testset
include("../day_02/code.jl")

@testset "day2" begin
    @testset "run_program_1" begin
        @test intcode([1,0,0,0,99]) == [2,0,0,0,99]
        @test intcode([2,3,0,3,99]) == [2,3,0,6,99]
        @test intcode([2,4,4,5,99,0]) == [2,4,4,5,99,9801]
        @test intcode([1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]
    end
end