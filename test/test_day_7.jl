import Test: @test, @testset
include("../day_07/code.jl")

@testset "day7" begin
    @testset "intcode_test" begin
        @test run_intcode([3,9,8,9,10,9,4,9,99,-1,8], 9) == 0
        @test run_intcode([3,9,8,9,10,9,4,9,99,-1,8], 8) == 1
        @test run_intcode([3,3,1108,-1,8,3,4,3,99], 9) == 0
        @test run_intcode([3,3,1108,-1,8,3,4,3,99], 8) == 1
        @test run_intcode([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0) == 0
        @test run_intcode([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 1) == 1
        @test run_intcode([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 2) == 1
        @test run_intcode([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 0) == 0
        @test run_intcode([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 1) == 1
        @test run_intcode([3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 2) == 1
        @test run_intcode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 8) == 1000
        @test run_intcode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 7) == 999
        @test run_intcode([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99], 9) == 1001
    end

    @testset "part1" begin
        @test get_thruster([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0], [4,3,2,1,0]) == 43210
        @test get_thruster([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0], [0,1,2,3,4]) == 54321
        @test get_thruster([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0], [1,0,4,3,2]) == 65210
        @test maximize_thruster([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]) == 43210
        @test maximize_thruster([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0]) == 54321
        @test maximize_thruster([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]) == 65210 
    end
    @testset "part2" begin
        @test feedback_thruster([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5], [9,8,7,6,5]) == 139629729
        @test feedback_thruster([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10], [9,7,8,5,6]) == 18216
        @test maximize_feedback_thruster([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5]) == 139629729
        @test maximize_feedback_thruster([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]) == 18216

    end
end